<?php 
require "utils/utils.php";
require "utils/File.php";
require "entity/Asociado.php";

$name = "";
$message = "";
$description = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  if (empty($_POST['nombre'])) {
    $errores []= 'Introduce tu nombre';
  } else {
    try {
      $name = trim(htmlspecialchars($_POST["nombre"]));
      $description = trim(htmlspecialchars($_POST["descripcion"]));
      $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
      $imagen = new File("imagen", $tiposAceptados);
      $imagen->saveUploadFile(Asociado::RUTA_IMAGE_LOGO);
      $message = "Datos enviados";
    } catch (FileException $fileException) {
      $errores []= $fileException->getMessage();
    }
  }
}

require 'app/views/asociados.view.php';
?>
