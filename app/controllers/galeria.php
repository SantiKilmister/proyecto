<?php 
//requires
require_once "core/App.php";

require_once  "utils/utils.php";
require_once "utils/File.php";

require_once "exceptions/FileException.php";
require_once "exceptions/QueryException.php";
require_once "exceptions/AppException.php";

require_once "entity/ImagenGaleria.php";
require_once "entity/Categoria.php";

require_once "database/Connection.php";
require_once "database/QueryBuilder.php";
require_once "database/IEntity.php";

require_once "repository/ImagenGaleriaRepository.php";
require_once "repository/CategoriaRepository.php";



$mensaje = "Correcto";
$errores = Array();
try {
  $connection = App::getConnection();

  $imagenGaleriaRepository = new ImagenGaleriaRepository();

  $categoriaRepository = new CategoriaRepository(); 

if ($_SERVER["REQUEST_METHOD"]==="POST") {

        $categoria = trim(htmlspecialchars($_POST["categoria"]));
        $descripcion = trim(htmlspecialchars($_POST["descripcion"]));
        $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];
        $imagen = new File("imagen", $tiposAceptados); 
        $mensaje = "Datos enviados";

        $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_GALLERY);
        $imagen->copyFile(ImagenGaleria::RUTA_IMAGENES_GALLERY,ImagenGaleria::RUTA_IMAGENES_PORTFOLIO);

        $imagenGaleria = new ImagenGaleria($imagen->getFileName(), $descripcion, $categoria);
        $nombre = $imagen->getFileName();

        $imagenGaleriaRepository->save($imagenGaleria);

            //Establecemos conexion
            
            // INSERTAR

            //$insertSQL = 'insert into imagenes (nombre, descripcion,categoria) values (:nombre, :descripcion,:categoria);';
            // Evitar inyecciones Sql

            /*

            $SQLInjections = $connection->prepare($insertSQL);
            $SQLInjections->execute(array(
              ':nombre' => $nombre,
              ':descripcion' => $descripcion,
              ':categoria' => $categoria
            ));

            */
        }

    $arrayImagenes = $imagenGaleriaRepository->findAll(); 

    $categorias = $categoriaRepository->findAll();


      }catch (FileException $fileException) {

        $errores [] = $fileException->getMessage();

    }

    catch (QueryException $queryException) {
        $errores [] = $queryException->getMessage();
      }
    

      catch ( AppException $AppException) {
        $errores [] = $AppException->getMessage();
      }
    

require  "app/views/galeria.view.php";

?>