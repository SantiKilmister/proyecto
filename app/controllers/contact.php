<?php


$nombre = htmlentities($_POST['nombre'] ?? null);
$apellidos = htmlentities($_POST['apellidos'] ?? null);
$email = htmlentities($_POST['email'] ?? null);
$asunto = htmlentities($_POST['asunto'] ?? null);
$mensaje = htmlentities($_POST['mensaje'] ?? null);
$error = "";

$errores = Array();

if($nombre == null){

    
    $error = "campo vacio en Nombre";
    array_push($errores,$error);
}



if($email == null ){
    $error = "campo vacio en email";
    array_push($errores,$error);

}else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){

    $error = "Correo no valido";
    array_push($errores,$error);
}

if($asunto == null){

    $error = "campo vacio en asunto";
    array_push($errores,$error);
}

			   


require "utils/utils.php";
require "app/views/contact.view.php";

