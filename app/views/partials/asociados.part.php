<!-- Box within partners name and logo -->
<div class="last-box row">
        <div class="col-xs-12 col-sm-4 col-sm-push-4 last-block">
        <div class="partner-box text-center">
          <p>
          <i class="fa fa-map-marker fa-2x sr-icons"></i> 
          <span class="text-muted">35 North Drive, Adroukpape, PY 88105, Agoe Telessou</span>
          </p>
          <h4>Our Main Partners</h4>
          <hr>
          <div class="text-muted text-left">
          <?php 
            //empieza for each
          $asociados;
          foreach($asociados as $key){
            ?>
            <ul class="list-inline">
              <li><img style="54px" src="../../images/index/asociados/log<?= $key->getLogo(); ?>.jpg" alt="<?= $key->getDescripcion(); ?>"></li>
              <li><?= $key->getNombre();?></li>
            </ul>
            <?php 
            //acaba for each
          }
            ?>
          </div>
        </div>
        </div>
      </div>
    <!-- End of Box within partners name and logo -->
