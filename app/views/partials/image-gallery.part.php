
<!-- First Category pictures -->
           <div id="<?= $idCategoria; ?>" class="tab-pane <?php if ($activa == true) {
    echo " active";
}
           
           ?>" 
           
           >
              <div class="row popup-gallery">
              <?php
              foreach ($arrayImagenes as $imagen) {
                  ?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                <div class="sol">
                  <img class="img-responsive indexFotos" src="<?= $imagen ->getURLPortfolio(); ?>" alt="<?php  echo $imagen -> getDescripcion(); ?>">
                  <div class="behind">
                      <div class="head text-center">
                        <ul class="list-inline">
                          <li>
                            <a class="gallery" href="<?= $imagen ->getURLGallerry(); ?>" data-toggle="tooltip" data-original-title="Quick View">
                              <i class="fa fa-eye"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#" data-toggle="tooltip" data-original-title="Click if you like it">
                              <i class="fa fa-heart"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#" data-toggle="tooltip" data-original-title="Download">
                              <i class="fa fa-download"></i>
                            </a>
                          </li>
                          <li>
                            <a href="#" data-toggle="tooltip" data-original-title="More information">
                              <i class="fa fa-info"></i>
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div class="row box-content">
                        <ul class="list-inline text-center">
                          <li><i class="fa fa-eye"></i></li>
                          <li><i class="fa fa-heart"></i></li>
                          <li><i class="fa fa-download"></i> </li>
                        </ul>
                      </div>
                  </div>
                </div>
                </div> 
                <?php
              }
                
                ?>
                </div>
                </div>
          
        <!-- End of First category pictures -->