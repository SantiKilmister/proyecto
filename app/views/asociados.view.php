<?php
include __DIR__ . "/partials/inicio-doc.part.php";
include __DIR__ . "/partials/nav.part.php";
?>
  <!-- Principal Content Start -->
  <div id="index">
    <div class="container">
      <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <h1>ASOCIADOS</h1>
        <hr>
        <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
          <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
            <button type="button" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">x</span>
            </button>
            <?php if(empty($errores)) : ?>
              <p><?= $message ?></p>
            <?php else : ?>
              <ul>
                <?php foreach($errores as $error) : ?>
                  <li><?= $error ?></li>
                <?php endforeach; ?>
              </ul>
            <?php endif; ?>
          </div>
        <?php endif; ?>
        <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] ?>" method="POST" enctype="multipart/form-data">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <label class="label-control">Nombre</label>
            </div>
            <input type="text" class="form-control" name="nombre" aria-label="Username" aria-describedby="basic-addon1">
          </div>
          <br>
          <div class="form-group">
            <div class="col-xs-12">
              <label class="label-control">Logo</label>
              <input class="form-control-file" name="imagen" type="file">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <label class="label-control">Descripción</label>
              <textarea class="form-control" name="descripcion" value="<?= $description ?>"></textarea>
              <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- Principal Content Start -->
  <?php include __DIR__ . "/partials/fin-doc.part.php"; ?>
