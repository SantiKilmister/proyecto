<?php
// Importamos el partial que contiene la apertura del HTML y la etiqueca head con todos los link enlazados
include __DIR__ . "/partials/inicio-doc.part.php";

// Importamos el partial que contiene el menú de navegación
include __DIR__ . "/partials/nav.part.php";
?>
<!-- Principal Content Start -->
<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') :  ?>
            <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
                <?php if(empty($errores)) :  ?>
                <p><?= $mensaje  ?></p>
              <?php else : //condicional else ?>
                <ul>
                    <?php foreach($errores as $error) : ?>
                    <li><?= $error //muestra errores?></li>
                  <?php endforeach; //cierre del bucle foreach ?>
                </ul>
              <?php endif; ?>
            </div>
          <?php endif; ?>

            <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] //echo que imprime el nombre del archivo que está ejecutándose ?>" method="POST"
                enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                   <label class="label-control">Categoría</label>
                   <select class="form-control" name="categoria">
                     <?php foreach ($categorias as $categoria) : ?>
                     <option value="<?= $categoria->getId(); ?>"><?= $categoria->getNombre(); ?></option>
                     <?php endforeach; ?>
                   </select>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <textarea class="form-control" name="descripcion" value="<?= $descripcion  ?>"></textarea>
                        <button class="pull-right btn btn-lg sr-button">ENVIAR</button>
                    </div>
                </div>
            </form>
        </div>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Imagen</th>
              <th scope="col">Nombre</th>
              <th scope="col">Categoría</th>
              <th scope="col">Descripción</th>
              <th scope="col">Visualizaciones</th>
              <th scope="col">Likes</th>
              <th scope="col">Downloads</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($arrayImagenes as $imagen) {?>
            <tr>
              <td><?= $imagen->getId();?></td>
              <td>
                <img src="<?= ImagenGaleria::RUTA_IMAGENES_PORTFOLIO.$imagen->getNombre(); ?>" alt="<?= $imagen->getDescripcion(); ?>" class="f200px">
              </td>
              <td><?= $imagen->getNombre(); ?></td>
              <td><?= $imagenGaleriaRepository->getCategoria($imagen)->getNombre() ?></td>
              <td><?= $imagen->getDescripcion(); ?></td>
              <td><?= $imagen->getNumVisualizaciones(); ?></td>
              <td><?= $imagen->getNumLikes(); ?></td>
              <td><?= $imagen->getNumDownloads(); ?></td>
            </tr>
            <?php } ?>
          </tbody>
      </table>
    </div>
</div>
<!-- Principal Content End -->
<?php

include __DIR__ . "/partials/fin-doc.part.php";
?>