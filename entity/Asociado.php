<?php 
class Asociado{

    private $nombre;
    private $logo;
    private $descripcion;

    //constructor

    function __construct(string $nombre, string $logo, string $descripcion)
  {
    $this->nombre = $nombre;
    $this->logo = $logo;
    $this->descripcion = $descripcion;
  }

  // const

  const RUTA_IMAGE_LOGO = "images/index/asociados/";
// Get y setters

/**
  * Get the value of Nombre
  *
  * @return mixed
  */
  public function getNombre()
  {
    return $this->nombre;
  }

  /**
  * Set the value of Nombre
  *
  * @param mixed nombre
  *
  * @return self
  */
  public function setNombre($nombre)
  {
    $this->nombre = $nombre;
    return $this;
  }

  /**
  * Get the value of Logo
  *
  * @return mixed
  */
  public function getLogo()
  {
    return $this->logo;
  }

  /**
  * Set the value of Logo
  *
  * @param mixed logo
  *
  * @return self
  */
  public function setLogo($logo)
  {
    $this->logo = $logo;
    return $this;
  }

  /**
  * Get the value of Descripcion
  *
  * @return mixed
  */
  public function getDescripcion()
  {
    return $this->descripcion;
  }

  /**
  * Set the value of Descripcion
  *
  * @param mixed descripcion
  *
  * @return self
  */
  public function setDescripcion($descripcion)
  {
    $this->descripcion = $descripcion;
    return $this;
  }
}

?>