<?php
// Clase

require_once "database/IEntity.php";
class ImagenGaleria implements IEntity
{
    private $id;
    private $nombre;
    private $descripcion;
    private $numVisualizaciones;
    private $numLikes;
    private $numDownloads;
    private $categoria;

    // Constructor
    public function __construct($nombre = '', $descripcion = '', $categoria = 0, $numVisualizaciones = 0, $numLikes = 0, $numDownloads = 0)
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->numVisualizaciones = $numVisualizaciones;
        $this->numLikes = $numLikes;
        $this->numDownloads = $numDownloads;
        $this->categoria = $categoria;
    }

    // Constantes
    const RUTA_IMAGENES_PORTFOLIO = "images/index/portfolio/";
    const RUTA_IMAGENES_GALLERY = "images/index/gallery/";

    // Getters & Setters
    /**
    * Get the value of Nombre
    *
    * @return mixed
    */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
    * Set the value of Nombre
    *
    * @param mixed nombre
    *
    * @return self
    */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
    * Get the value of Descripcion
    *
    * @return mixed
    */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
    * Set the value of Descripcion
    *
    * @param mixed descripcion
    *
    * @return self
    */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
    * Get the value of Num Visualizaciones
    *
    * @return mixed
    */
    public function getNumVisualizaciones()
    {
        return $this->numVisualizaciones;
    }

    /**
    * Set the value of Num Visualizaciones
    *
    * @param mixed numVisualizaciones
    *
    * @return self
    */
    public function setNumVisualizaciones($numVisualizaciones)
    {
        $this->numVisualizaciones = $numVisualizaciones;
        return $this;
    }

    /**
    * Get the value of Num Likes
    *
    * @return mixed
    */
    public function getNumLikes()
    {
        return $this->numLikes;
    }

    /**
    * Set the value of Num Likes
    *
    * @param mixed numLikes
    *
    * @return self
    */
    public function setNumLikes($numLikes)
    {
        $this->numLikes = $numLikes;
        return $this;
    }

    /**
    * Get the value of Num Downloads
    *
    * @return mixed
    */
    public function getNumDownloads()
    {
        return $this->numDownloads;
    }

    /**
    * Set the value of Num Downloads
    *
    * @param mixed numDownloads
    *
    * @return self
    */
    public function setNumDownloads($numDownloads)
    {
        $this->numDownloads = $numDownloads;
        return $this;
    }

    // Métodos
    public function getURLPortfolio() : string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }
    public function getURLGallerry() : string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }

    /**
     * Get the value of Imagenid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Imagenid
     *
     * @return  self
     */
    public function setid($id)
    {
        $this->id = $id;

        return $this;
    }

    public function toArray(): array
    {
        return [
     "id"=>$this->getId(),
     "nombre"=>$this->getNombre(),
     "descripcion"=>$this->getDescripcion(),
     "numVisualizaciones"=>$this->getNumVisualizaciones(),
     "numLikes"=>$this->getNumLikes(),
     "numDownloads"=>$this->getNumDownloads(),
     "categoria"=>$this->getCategoria()
    ];
    }

    /**
     * Get the value of Categoria
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of Categoria
     *
     * @return  self
     */
    public function setCategoria($Categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }
}
