<?php

class File
{
    private $file;



    private $fileName;

    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if (($this->file["name"] =="")) {

            //TODO: Error
            throw new FileException("Debes especificar un fichero", 1);
        }

        if ($this->file["error"] !== UPLOAD_ERR_OK) {
            switch ($this-file["error"]) {

                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    //TODO: Error de tamaño
                    throw new FileException("Error de tamaño");

                case UPLOAD_ERR_PARTIAL:

                    //TODO: Error de archivo incompleto
                    throw new FileException("Archivo incompleto");

                default:

                    // TODO: error genérico en la súbida del fichero
                    throw new FileException("Error genérico en la subida del fichero");

                    break;

            }
        }

        if (in_array($this->file["type"], $arrTypes)===false) {

            //TODO error de tipo
            throw new FileException("Error en el formato", 1);
        }
    }


    /**
     * Get the value of fileName
     */
    public function getFileName()
    {
        return $this->file['name'];
    }

    public function saveUploadFile($ruta)
    {
        if (is_uploaded_file($this->file['tmp_name'])) {
            $archivoRuta = $ruta.$this->file['name'];
        

            if (is_file($archivoRuta)) {
                $idUnico = time();

                $this->file['name'] = $idUnico."_".$this->file['name'];
                $archivoRuta = $ruta.$this->file['name'];
            }

            if (move_uploaded_file($this->file['tmp_name'], $archivoRuta) === false) {
                throw new FileException("No se pudo mover el archivo deseado");
            }
        } else {
            throw new FileException("El archivo no se ha subido mediante un formulario.");
        }
    }

    public function copyFile($origen, $destino)
    {
        $origen = $origen.$this->file['name'];
        $destino = $destino.$this->file['name'];

        if (is_file($origen) == true) {
            if (is_file($destino) == false) {
                if (copy($origen, $destino) == false) {
                    throw new FileException("El fichero ya existe");
                }
            } else {
                throw new FileException("El fichero de origen no fue encontrado");
            }
        }
    }
}
